package example.honey.fullpagecalendar.interfaces;

import java.util.Calendar;

public interface CalenderClick {
  void onMonthClick(Calendar clickedMonthCalendarObj);
}
