package example.honey.fullpagecalendar.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import example.honey.fullpagecalendar.R;
import example.honey.fullpagecalendar.adapter.FullPageCalendarViewAdapter;
import example.honey.fullpagecalendar.interfaces.CalenderClick;
import example.honey.fullpagecalendar.interfaces.RecyclerItemClickListener;
import example.honey.fullpagecalendar.pojo.CalanderObject;


public class FullPageCalendarView extends LinearLayout {

    private RecyclerView mMonthList;
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    public CalenderClick calenderClickEvent;
    ArrayList<CalanderObject> monthList = new ArrayList<>();

    public FullPageCalendarView(Context context) {
        super(context);
        init();
    }

    public FullPageCalendarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int sizeAdjustPadding = 80;
        int cols = 4;
        setRecyclerView(h/cols - sizeAdjustPadding);

    }

    private void init() {
        inflate(getContext(), R.layout.view_custom_calendar, this);

        // Set Calendar to 1st Jan of CurrentYear
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH,1);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initView();
    }

    private void initView() {
        mMonthList = findViewById(R.id.monthList);

        mMonthList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mMonthList ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override public void onItemClick(View view, int position) {
                // do whatever

                if(calenderClickEvent!=null){
                    CalanderObject calObj = monthList.get(position);
                    Calendar obtainedCal = Calendar.getInstance();
                    obtainedCal.setTime(new Date(calObj.getCurrentMonth()));

                    if(calenderClickEvent!=null)
                        calenderClickEvent.onMonthClick(obtainedCal);
                }

            }

            @Override public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    private void setRecyclerView(int gridHeight){

        mMonthList.setLayoutManager(new GridLayoutManager(getContext(),3));
        mMonthList.setAdapter(new FullPageCalendarViewAdapter(getCalendarList(), gridHeight));
    }

    private ArrayList<CalanderObject> getCalendarList(){


        for(int i=0;i<12;i++){

            Calendar mCal = (Calendar) cal.clone();
            CalanderObject calObj = new CalanderObject();

            calObj.setCurrentMonth(mCal.getTime().toString());
            calObj.setMonthName(new SimpleDateFormat("MMMM",Locale.getDefault()).format(cal.getTime()));
            monthList.add(calObj);

            cal.add(Calendar.MONTH,1);
        }

        return monthList;
    }
}
