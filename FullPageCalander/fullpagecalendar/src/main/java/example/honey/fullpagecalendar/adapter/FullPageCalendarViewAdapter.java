package example.honey.fullpagecalendar.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import example.honey.fullpagecalendar.R;
import example.honey.fullpagecalendar.pojo.CalanderObject;

import static android.content.ContentValues.TAG;

public class FullPageCalendarViewAdapter extends RecyclerView.Adapter<FullPageCalendarViewAdapter.MyViewHolder> {

    private static final int MAX_CALENDAR_COLUMN = 42;

    private ArrayList<CalanderObject> monthList;
    private int GRID_HEIGHT;


    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mMonthName;
        private GridView mDates;


        MyViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View convertView) {
            mMonthName = convertView.findViewById(R.id.monthName);
            mDates = convertView.findViewById(R.id.dates);


        }

    }

    public FullPageCalendarViewAdapter(ArrayList<CalanderObject> monthList, int GRID_HEIGHT) {
        this.monthList = monthList;
        this.GRID_HEIGHT = GRID_HEIGHT;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_custom_calendar_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        int pos = holder.getAdapterPosition();
        holder.mMonthName.setText(monthList.get(pos).getMonthName());

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date(monthList.get(pos).getCurrentMonth()));
        holder.mDates.setAdapter(new DateAdapter(holder.itemView.getContext(),setUpCalendarAdapter(currentDate),currentDate));

        // highlight current month

        Calendar currentTime = Calendar.getInstance();

        String currentMonth = new SimpleDateFormat("MMMM", Locale.getDefault()).format(currentTime.getTime());

        if(currentMonth.equals(holder.mMonthName.getText().toString())){
            holder.mMonthName.setTextColor(Color.BLUE);
        } else{
            holder.mMonthName.setTextColor(Color.BLACK);
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,GRID_HEIGHT);
        params.setMargins(0,5,0,0);
        holder.mDates.setLayoutParams(params);
        holder.mDates.requestLayout();

    }

    @Override
    public int getItemCount() {
        return monthList.size();
    }

    private ArrayList<Date> setUpCalendarAdapter(Calendar cal){

        ArrayList<Date> dayValueInCells = new ArrayList<>();

        Calendar mCal = (Calendar)cal.clone();
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while(dayValueInCells.size() < MAX_CALENDAR_COLUMN){
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());

        return dayValueInCells;
    }
}