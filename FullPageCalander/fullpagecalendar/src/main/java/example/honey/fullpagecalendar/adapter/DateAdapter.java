package example.honey.fullpagecalendar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import example.honey.fullpagecalendar.R;

public class DateAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Date> monthlyDates;
    private Calendar currentDate;

    DateAdapter(Context context, ArrayList<Date> monthlyDates, Calendar currentDate) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        mInflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        Date mDate = monthlyDates.get(position);

        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);

        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) +1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) +1;
        int currentYear = currentDate.get(Calendar.YEAR);


        View view = convertView;
        if(view == null){
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }

        view.setBackgroundColor(Color.parseColor("#ffffff"));

        //Add day to calendar
        TextView cellNumber = view.findViewById(R.id.calendar_date_id);
        cellNumber.setText(String.valueOf(dayValue));


        if(displayMonth == currentMonth && displayYear == currentYear){
            cellNumber.setVisibility(View.VISIBLE);

            // Compare for currentDate
            if(dayValue==Calendar.getInstance().get(Calendar.DAY_OF_MONTH) &&
                    currentMonth ==( Calendar.getInstance().get(Calendar.MONTH)+1)){
                cellNumber.setTextColor(Color.BLUE);
            } else{
                cellNumber.setTextColor(Color.BLACK);
            }
        }else{
            cellNumber.setVisibility(View.GONE);
        }

        return view;
    }
    @Override
    public int getCount() {
        return monthlyDates.size();
    }
    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }
    @Override
    public int getPosition(Object item) {
        //noinspection SuspiciousMethodCalls
        return monthlyDates.indexOf(item);
    }
}