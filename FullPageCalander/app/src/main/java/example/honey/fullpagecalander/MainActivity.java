package example.honey.fullpagecalander;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;

import example.honey.fullpagecalendar.interfaces.CalenderClick;
import example.honey.fullpagecalendar.view.FullPageCalendarView;

public class MainActivity extends AppCompatActivity {

    private FullPageCalendarView mCalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mCalendarView = findViewById(R.id.calendarView);


        mCalendarView.calenderClickEvent = new CalenderClick() {

            @Override
            public void onMonthClick(Calendar clickedMonthCalendarObj) {

            }
        };
    }
}
